#!/usr/bin/ruby
require 'vkontakte_api'

VkontakteApi.configure do |config|
#not used in sandbox
#   config.app_id       = '5568407'
#   config.app_secret   = 'KBwt3YmYDcgKWquRXX8D'
#   config.redirect_uri = 'http://api.vkontakte.ru/blank.html'
 config.log_requests    = false
 config.log_errors      = false
 config.log_responses   = false
end







class App



  def users_get
    args_cycle {|id|puts vk.users.get(user_ids:[id])}
  end

  def friends_get
    args_cycle {|id|puts vk.friends.get user_id:id}
  end

private

  def vk
    @vk ||= VkontakteApi::Client.new
  end

  def args_cycle
    no_args = ARGV.empty?
    while id =  unless no_args then ARGV.shift else ARGF.gets end
      yield id
    end
  end

end

App.new.send(ARGV.shift.to_sym)# rescue NoMethodError