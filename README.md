# Piper
1. Thing to be used for building bash/ruby/ pipelines
looks like the last chance language here is ruby, buththere are several another language folders - elisp and haskell - by historical reasons

+/- shell command builder https://github.com/thoughtbot/cocaine

2. Console in X

3. Data flow, 
list of standard operations
 - filter = grep
 - column = awk
 - join with variants (intersect, complement, union)flow

terminology
 - graph - whole data flow scheme with nodes and edges
 - operation - node in graph, process for data converting/getting/sending
 - (data)flow - edge in graph, data sending between operations
 - pipe(line) - non-branchy part of the graph, consists of nodes and edges between them
 - message - data sent in flows
 - back - variant of running, bash/ruby/go/etc.

commands written in pure bash - http://stackoverflow.com/questions/3844903/how-to-extend-bash-shell
gems for command line apps
 - http://www.awesomecommandlineapps.com/gems.html
 - https://www.ruby-toolbox.com/categories/CLI_Option_Parsers

+++ modular extendable irb https://github.com/cldwalker/ripl!!!