require 'auto_virus'

module Splitter
  VERSION = "0.0.1"

  def self.build opts = {type: 'window', children: [{type: 'terminal'}]}
    Node.from_json(opts)
  end

  def self.dsl &block
    Builder.new.instance_eval &block
  end


  def self.app &block
    Gtk3::App.new {|app|  build dsl &block }
  end

end

AutoVirus.infect Splitter


# scr = Splitter.dsl{
#   window{
#     hsplit{[text,terminal]}
#   }
# }
# tabs = scr.children.first
# term = tabs.children.last
#
# tabs2 = Splitter::Node.new type: 'tabs'
# term3 = Splitter.dsl{terminal}
#
#
# Thread.abort_on_exception = true
# Thread.new{ sleep 1; term.insert_parent tabs2, term3;}
#
#
#
# scr.place.run