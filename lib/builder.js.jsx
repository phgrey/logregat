//several tree-building alghoritms

class Builder{
    tree(parent, node){
        node.parent = parent
        var binded = this.tree.bind(this, node)
        return [this, node.children(parent).map(binded)]
    }

    leaves(parent, node){
        node.parent = parent
        var binded = this.tree.bind(this, parent)
        return node.children(parent).map(binded)
    }

    down_first(params){
        params = this.process(params)
        return this.parent ? this.parent.down_first(res) : res
    }

    down_last(params){
        if(this.parent) params = this.parent.down_last(params)
        return this.process(params)
    }
}
