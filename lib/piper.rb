require 'auto_virus'

module Piper
  VERSION = "0.0.1" unless defined?(::Piper::VERSION)

  def self.dsl(&block)
    DSL.run &block
  end
end

AutoVirus.infect Piper