module AutoVirus
  #asd_as => AsdAs, unused
  # def self.camelcase fname
  #   fname.split('_').map(&:capitalize).join
  # end
  VERSION = "0.0.1"

  def self.underscore mname
    mname.gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').gsub(/([a-z\d])([A-Z])/,'\1_\2').downcase
  end

  def self.file_name module_name
    module_name.split('::').map(&method(:underscore)).join '/'
  end

  def self.infect victim
    def victim.const_missing subname
      full = "#{name}::#{subname}"
      file = AutoVirus.file_name full
      raise ::LoadError, "#{full} was not found in #{file}" unless require file
      AutoVirus.infect const_get subname
    end
    victim
  end


  # TODO: compare
  # module Virus
  #   def self.extended victim
  #     def victim.const_missing subname
  #       fullname = [name, subname.to_s].join '::'
  #       require Autoload.file_name fullname
  #       const_get(fullname).extend Virus
  #     rescue ::LoadError
  #       raise ::NameError("Undefined constamt #{fullname}")
  #     end
  #     victim
  #   end
  # end
  #
  # Piper.extend Virus
end