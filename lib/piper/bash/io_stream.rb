require 'io'
require 'thread'

module Piper
  module Bash
    class IoStream
      include ::Piper::Graph::Edge
      attr_reader :link_in, :link_out


      def initialize
        @link_out, @link_in = IO.pipe.reverse
        @copiers = []
      end

      def start
        @copiers.each &:start
      end

      def to_glue src, trg
        @copiers.push IoCopy.new src, trg
      end



      # @param source Node
      def input= source
        if source.respond_to? :link_out=
          source.link_out = link_in
        elsif  source.respond_to? :link_out
          to_glue source.link_out, link_in
        else
          raise 'Can not connect to source'
        end
      end


      def output= target
        if target.respond_to? :link_in=
          target.link_in = link_out
        elsif  target.respond_to? :link_in
          to_glue link_out, target.link_in
        else
          raise 'Can not connect to source'
        end
      end

    end


  end

  class IoCopy < Thread

    def initialize src, trg
      @src, @trg = src, trg
      super &method(:start_stopped)
    end

    def start_stopped
      Thread.stop
      transfer
    end

    def transfer
      @trg.write_nonblock @src.read_nonblock 1000
    rescue IO::WaitReadable
      IO.select([@src])
      retry
    rescue IO::WaitWritable
      IO.select(nil, [@trg])
      retry
    end

  end
end