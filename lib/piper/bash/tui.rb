# Terminal user emulator things

require 'pty'
require_relative 'process'


module Piper
  module Bash
    class Tui < Process
      def initialize cmd = 'bash'
        @cmd = cmd
      end

      def start
        raise ScriptError unless link_in && link_out
        @pid = ::Process.pid
      end


      def link_in
        $stdin
      end

      def link_out
        $stdout
      end

    end
  end
end



