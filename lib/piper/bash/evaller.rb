require 'piper/ruby/process'


module Piper
  module Bash
    class Evaller < Process
      include Piper::Graph::Node
      def initialize cmd = 'bash'
        @cmd = cmd
      end

      def start
        raise 'Not enough params to start' unless @cmd && link_in && link_out
        @pid = spawn @cmd, in: link_in, out: link_out
      end
    end
  end
end