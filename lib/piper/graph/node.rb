module Piper
  module Graph
    module Node


      def |(node)
        Graph.new(nodes:[self]) | node
      end

    end
  end
end