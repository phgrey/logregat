

module Piper
  class DSL


    def initialize
      @lines = {}
      @builder = nil
    end

    def run(&block)
      return instance_eval &block if block_given?

      Dir.chdir('..') until File.exist? './Pipefile' || Dir.pwd == '/'
      instance_eval File.read './Pipefile'
      # puts @tasks.last, `#{@tasks.last}` if @tasks.length > 0
    end




    def line(name = nil, &block)
      name = "line#{@lines.length}" if name.nil?
      @lines[name] = @builder.instance_eval &block
    end


    def type= new_type
      @builder = Piper.const_get(new_type.to_s.capitalize + '::Builder').new
    end



  end
end