# Abstract layer - to implement solId
module Piper
  class Graph

    attr_reader :nodes

    def initialize(options = {})
      @nodes = options[:nodes] || []
      @links = {}
    end

    def |(node)
      case node
        when Graph
          link(@nodes.last, node.nodes.first)
          @nodes += node.nodes
        when Graph::Node
          link(@nodes.last, node)
          @nodes << node
        else
          raise 'Only Graph and Node objects are allowed'
      end
      self
    end


    private

    def link_nodes src, dest
      @links[src] = dest
    end
  end
end