require 'gtk3'
require 'vte3'

module Splitter::Gtk3

  module Common
    attr_accessor :back
  end

  module Element
    include Common
  end

  module Container
    include Common
    def resplit old, new
      #http://stackoverflow.com/questions/27343166/gtk3-replace-child-widget-with-another-widget
      props = self.class.child_properties.map{|key|[key.name, child_get_property(old, key.name)]}
      remove old
      add new
      props.each{|name, val| child_set_property new, name, val;}
    end
  end

  class Hsplit < ::Gtk::Paned
    include Container
    @split_type = :horizontal
    class << self
      attr_reader :split_type
    end
    def initialize
      @split_actual = [:pack1, :pack2]
      super self.class.split_type
    end

    def split what
      send @split_actual.shift, what if @split_actual.length > 0
    end
    alias unsplit remove

  end

  class Vsplit < Hsplit
    @split_type = :vertical
  end

  class Tabs < ::Gtk::Notebook
    include Container
    def split what
      lab = Gtk::Label.new what.window_title
      append_page what, lab
      what.signal_connect("window-title-changed") { |widget| lab.text = widget.window_title}
    end

    alias unsplit detach_tab
  end


  class Text < ::Gtk::TextView
    include Element
    def window_title
      'olololo'
    end
  end

  class Window < ::Gtk::ApplicationWindow
    include Container
    def initialize title='Piper'
      super App.last
      set_size_request(400, 400)
      set_title 'Piper'
      maximize
      signal_connect("delete-event", &:unsplit)
    end

    alias split add

    def unsplit what
      application.quit
    end

  end

  class App < ::Gtk::Application
    def self.last
      @@last
    end
    def initialize &block
      super("org.phgrey.spitter", :flags_none)
      @@last = self
      signal_connect "activate" do |app|
        block.call
        app.windows.first.present
        app.windows.first.show_all
      end
    end
  end

end
