module Splitter::Gtk3
  class Terminal < ::Gtk::Box
    include Element
    def initialize
      super(:vertical, 0)
      @term = ::Vte::Terminal.new
      @term.spawn({})
      @term.signal_connect("child-exited"){|w| back.remove}

      pack_start panel

      box2 = ::Gtk::Box.new :horizontal, 0
      box2.pack_start @term, :expand => true, :fill => true, :padding => 0
      box2.pack_start ::Gtk::Scrollbar.new(:vertical, @term.vadjustment)

      pack_start box2, :expand => true, :fill => true, :padding => 0
      pack_start ::Gtk::Scrollbar.new(:horizontal, @term.hadjustment)

    end

    def panel
      panel = ::Gtk::Toolbar.new
      panel.icon_size=  'small-toolbar'
      [
          ['Split Vertical', 'object-flip-vertical', :remove],
          ['Split Horizontal', 'object-flip-horizontal', :remove],
      ].each do |label, icon, action|

        action1 = ::Gtk::ToolButton.new label:label
        action1.icon_name= icon
        # action1.signal_connect('clicked', back.method(action))
        panel.insert action1, 0
      end
      # p Hash[panel.class.properties.map{|name|[name, panel.get_property(name)]}]
      panel
    end
  end
end
