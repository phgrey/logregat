module Splitter
  class Builder
    def method_missing(name, **kvargs, &block)
      kvargs[:type] = name
      kvargs[:children] = instance_eval &block if block_given?
      kvargs
    end
  end
end

# p Splitter::Builder.new.instance_eval(){
#   screen(otp: 12){[
#     terminal,
#     terminal
#   ]}
# }