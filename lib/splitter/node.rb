module Splitter
  class Node
    attr_reader :place

    def self.from_json opts
      child_opts = opts.delete(:children) || []
      child_opts = [child_opts] unless child_opts.is_a? Array
      el = new opts
      child_opts.each{|o| el.add_child from_json o}
      el
    end

    def place_for type
      Splitter::Gtk3.const_get type.capitalize.to_sym
    end


    def initialize opts
      super()
      @place = place_for(opts[:type]).new
      @place.back = self
      @children = []
      @opts = opts
    end

    attr_accessor :parent, :children

    def add_child node
      @children << node
      node.parent = self
      @place.split node.place
    end

    def remove_child node
      @children.delete node
      if @children.length == 1
        @place.unsplit @children.first.place
        @parent.replace_child self, @children.first
        @place.destroy
      else
        @place.unsplit node.place
      end
    end

    def remove
      @parent.remove_child self
      @place.destroy
    end



    def replace_child old, new
      ind = @children.index old
      @children[ind] = new
      new.parent = self
      @place.resplit old.place, new.place
    end

    def insert_parent node, sibling
      @parent.replace_child self, node
      node.add_child self
      node.add_child sibling
      @parent.place.show_all
    end

  end
end