require 'piper'

RSpec.describe Piper do
  it "creates single pipeline" do
    cmds = Piper.dsl { type :bash; line {intersect(vk(:friends_get, 5623401), vk(:friends_get, 179987111)) | vk(:users_get)} }
    expect(order.total).to eq(Money.new(5.55, :USD))
  end
end