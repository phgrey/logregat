;;
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Font-Lock-Basics.html#Font-Lock-Basics
;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Defining-Faces.html#Defining-Faces
;; well, fontlock is used a bit stupid way - we do not obtain a buttons, but only a text highlighting
(require 'logregat-pipe)


(require 'popup)


;;RUN cmds,
;;TODO: async run of the comand
;;http://ergoemacs.org/emacs/elisp_idioms_batch.html
;;https://www.gnu.org/software/emacs/manual/html_node/elisp/Synchronous-Processes.html

;;TODO: processes status after run, e.g. get them by group
;; `pstree -lapgs 4802` - show ancestors up to root
;; `pstree -lapg 4469` - show tree of subprocesses

(defun logregat-run(cmd)
  (let ((plain-cmd (logssh-compile cmd)))
    ;;(insert (shell-command-to-string plain-cmd));sync
    ;;(print plain-cmd);DEBUG to messages
    (start-process-shell-command (format "l-%s" cmd) (buffer-name) plain-cmd)
  ))

(defun logregat-run-simple(cmd)
  "Simple compile logssh sequence to bash cmd, run it and return the result"
  (let ((plain-cmd (logssh-compile cmd))
	(inhibit-read-only t))
    (princ plain-cmd);DEBUG to messages
    (insert (shell-command-to-string plain-cmd))))


;;Listeners, hooks, modes
;;TODO default value for source https://www.gnu.org/software/emacs/manual/html_node/elisp/Using-Interactive.html
(defun logregat-set-source(s)
  ;;(interactive "xSource: ")
  (interactive (list (read (read-string "Source: " (format "%S" logregat-source)))))
  (setq logregat-source s);(read-from-minibuffer "Source query: " source))
  (when logregat-source (logregat-load logregat-source)))
  
(defun logregat-load(source)
  (logregat-insert (format "%S"  source) 'source)
  (logregat-run source))

(defun logregat-insert(text &optional type)
  (let ((inhibit-read-only t)
	(start (point)))
    (insert (format "%s" text))
    (when type
      (add-text-properties start (point) `(log-object ,type
				           log-value  ,text
					   mouse-face highlight	      
					   face       link)))
  ))

(defun logregat(&optional s)
  (interactive)
  ;(with-current-buffer (get-buffer-create "*gregat*")
  (switch-to-buffer-other-window "*gregat*")
  (logregat-results-mode)
  (when s (setq logregat-source s))
  (logregat-load))

;; Dynamically bound? not sure able to operate with for now
;;(defvar logregat-source

(defun logregat--init-mode()
  (define-derived-mode logregat-results-mode special-mode
    (setq email-style '(face "link"))
    (setq logresult-keywords '(("[^ ]+@[^ ]+" . email-style)))
    (setq font-lock-defaults '(logresult-keywords))
    
    (setq mode-name "logresults")

    (make-local-variable 'logregat-source)
    (setq logregat-source nil)

    (setq header-line-format '("%1 " (:eval (format "%s" logregat-source)) "%1 %s"))
    ;;(local-set-key [header-line mouse-1] 'logregat-set-source)
    (local-set-key [mouse-1] 'logregat-show-menu)
    ))


(defun logregat-clean()
  
  (let ((inhibit-read-only t))
    (erase-buffer))
  
  )

(defun logregat-click-menu(log-object action value)
  

(defun logregat-show-menu(&optional e)
  (interactive)
  (let* ((log-object (get-text-property (point) 'log-object))
	 (actions (when log-object (gethash log-object logregat-menu)))
	 (action (when actions (popup-menu* actions )))
	 (aname (concat-syms log-object '- action) )
	 (func-name (concat-syms 'logregat-menu- aname) ))
    (when action
      (cond ((fboundp func-name)
	     (funcall func-name (get-text-property (point) 'log-value )))
	    ((gethash aname 
      ))))

;;This important part of data model - list of actions available for object type

(setq logregat-menu #s(hash-table size 10 data (
	;;source ((run . logregat-run-simple) (start . logregat-run) (resume . logregat-resume ) (stop . logregat-stop))
	source (run start resume stop)
	email (status log lates)					
	)))

;;And the actions value itself

(defun logregat-menu-source-run(source) (logregat-run-simple source))
(defun logregat-menu-source-start(source) (logregat-run source))
(defun logregat-menu-source-stop(source) (interrupt-process))
;;https://www.gnu.org/software/emacs/manual/html_node/elisp/Output-from-Processes.html
;;https://www.gnu.org/software/emacs/manual/html_node/elisp/Process-Buffers.html
;;TODO: notice there can be several processes runned at once with the buffer
(defun logregat-menu-source-resume(source) (accept-process-output (get-buffer-process (buffer-name))))

(defun logregat-menu-email-status(email)
  (logregat-run `(templates ,email "SELECT email, taken_by, taken_at FROM queue WHERE email = %S;" mysql on-beta )))



(provide 'logregat-show)
