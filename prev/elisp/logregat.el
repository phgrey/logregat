;; Tutorial
;; http://emacs-doctor.com/learn-emacs-lisp-in-15-minutes.html
;; http://ergoemacs.org/emacs/elisp_editing_basics.html
;; 
;;
;; Tables:
;;    nice and complicated - https://github.com/kiwanami/emacs-ctable
;;    basic - https://www.emacswiki.org/emacs/TableMode
;;    java - 
;;
;; Filter
;;    https://github.com/mhayashi1120/Emacs-wgrep

(require 'logregat-init)

;;CMDS structure
(require 'logregat-pipe)

;;SHOW
(require 'logregat-show)
  
  

;;WORKFLOW
(logregat '(on-beta (mysql "SHOW TABLES;")))
(progn
  (switch-to-buffer-other-window "*gregat*")
  (other-window 1))

;load-path
