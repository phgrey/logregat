;;UTILS used below
(defun concat-syms (&rest syms)
  "symbol concatenating tool"
  (intern (apply #'concat (mapcar #'symbol-name syms))))


(defun sym-fboundp(symb)
  "(and (symbolp symb) (fboundp symb)))"
  (and (symbolp symb) (fboundp symb)))



(defun quote-shell(shell)
  (let* ((shell (replace-regexp-in-string "/\\/" "\\\\" shell))
	 ;;(shell (replace-regexp-in-string "\"" "\\\"" shell t t))
	 (shell (replace-regexp-in-string "'" "\'\"\'\"\'" shell t t)))
    (concat "'" shell "'")))

   
;;TODO: uuencode / decode instead of ecranning
;;TODO not only first argument can be a templatable %B...
(defun format-any(template &optional cobj &rest objects)
  "Here will try to insert a bash arguments to the template"
  (let ((first-pos (string-match "%[a-zA-Z]" template)))
    ;;.. by inserting while instead of if here
    (if (eq first-pos nil) (apply 'format template template cobj objects)
      (if (eq ?B (elt template (+ first-pos 1) ))
	  ;;here is our fucking case - all the function is written just for this fucking string
	  (let ((template2 (concat template)))
	    (aset template2 (+ first-pos 1) ?s) 
	    (apply 'format template2 (quote-shell cobj) objects))
	(apply 'format template cobj objects))
    )
))

;;should eq to 
;;(format-any "ssh smartback.beta %B" "echo \"hello \"")

;(shell-quote-argument "echo \"hello \"")

;(logssh-compile '(on-beta ([head "~/my-que.list" |  awk "'{printf \"../imap/%s.log\\n\", $1}'"  ])))


;;SSH-commands are for translating
(setq logssh-dict #s(hash-table size 10 data (
      stdrun "%s 2>&1" ;; running most of commands
      ;;tools from logrape
      grep_email "grep -Eio '([[:alnum:]_.-]+@[[:alnum:]_.-]+?\.[[:alpha:].]{2,6})'"
      sort "LANG=en_EN sort"
      compare "LANG=en_EN join -v 1 %s %s"
      worker-log "/home/smartback/mailcloud/logs/worker.log"
      | " | " ;" 2>&1 | "
      > " > "
      && " && "
      ;;mysql - with ecranned \", no table drawed and without column names
      mysql "mysql smartback -sNe %S"
      on-beta "ssh smartback.beta %B")))

(defun logssh-compile-symbol(cmd)
  (gethash cmd logssh-dict (symbol-name cmd)))
  

(defun logssh-compile(cmd)
  (if (stringp cmd) cmd
    (logssh-compile (cond ((symbolp cmd) (logssh-compile-symbol cmd))
			  ((listp cmd) (apply 'logssh-compile-list cmd))
			  ((vectorp cmd) (apply 'logssh-concat (append cmd nil)));'(" 2>&1"))))
		    ))))
   
(defun logssh-compile-list(cmd &rest args)
  (let ((cmd2 (if (symbolp cmd)
		  (concat-syms 'logssh- cmd)
		nil)))
    (cond ((gethash cmd logssh-dict) (apply 'logssh-template cmd args))
	  ((sym-fboundp cmd2)        (apply cmd2 args))
	  ;; there are some functions - such as 'grep, that override needed mechanism
	  ((sym-fboundp cmd)         (apply cmd args))
	  (t                         (apply 'logssh-concat cmd args)))))
	  

;;TODO replace format %S with correct way of ecraning for bash-mysql-awk-grep
;;e.g. ssh ecraning - $1 => $$1
(defun logssh-template(&rest args)
  "Just a simple format function with additional predicate - %B for ecran-me-for-bash parameters"
  ;(apply 'format (mapcar 'shell-quote-argument (mapcar 'logssh-compile args))))
  (apply 'format-any (mapcar 'logssh-compile args)))


(defun logssh-templates(cmd &optional template &rest wrappers)
  (if template
      (apply 'logssh-templates (logssh-template template cmd) wrappers)
      cmd))
  
(defun logssh-concat(&rest cmds)
  (mapconcat 'logssh-compile cmds " "))
  
(defun logssh-pipe(&rest cmds)
  (mapconcat 'logssh-compile cmds " | "))

;;think about as a piping - query is sent at the mysql cmd stdin
(defun logssh-mysql-beta(query)
  `(templates ,query mysql stdrun on-beta stdrun))



;(mysql_beta "SELECT count(*) FROM accounts;")

(defun logssh-lost-emails()
  [(mysql "SELECT email, taken_by, taken_at FROM queue WHERE type='imap';") | sort > "~/my-que.list"
   && cat worker-log | grep "'^2016-04-01T'" | grep_email | sort | uniq | (compare "~/my-que.list" "-")
   | awk "'{printf \"/home/smartback/mailcloud/logs/imap/%s.log\\n\", $1}'" | xargs ls -l])

(defun logssh-watch-errors()
  [tailf worker-log | grep "error"])

;;(logssh-compile '(on-beta (lost-emails)))

;;(logssh-compile '(mysql-beta "SHOW TABLES;"))



(provide 'logregat-pipe)



(defun logregat-pipe-tests()
;; SOME tests onn quoting including awk
(logregat-run-simple '(on-beta [head "~/my-que.list"]))
(logregat-run-simple '(on-beta [echo "hello, \\\" double quote"]))
(logregat-run-simple '(on-beta (mysql "SHOW TABLES;")))

(logregat-run-simple '([head "~/my-que.list" |  awk "'{printf \"../imap/%s.log\\n\", $1}'"  ]))

(logregat-run-simple '(on-beta ([head "~/my-que.list" | awk "'{printf \"../imap/%s.log\\n\", $1}'"  ])))

(logregat-run-simple '(template "ssh smartback.beta %B" "echo \"hello \""))


(logssh-compile '(on-beta (mysql "SHOW TABLES;")))
(logssh-compile '(on-beta [head "~/my-que.list" |  awk "'{printf \"../imap/%s.log\\n\", $1}'"  ]))
)
