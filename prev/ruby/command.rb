#in fact this is a compiler from our internal representation (hierarhical strings)
# to a bash script ready to be runned

module Piper
  module Pipe

    PIPE = ' | '

    class Command < String

      @output_at = nil
      attr_accessor :output_at

      @@templates = {
          stdrun: "%s 2>&1",
          grep_email: "grep -Eio '([[:alnum:]_.-]+@[[:alnum:]_.-]+?\\.[[:alpha:].]{2,6})'",
          sort: "LANG=en_EN sort",
          compare: "LANG=en_EN join - %s",
          workerlog: "/home/smartback/mailcloud/logs/worker.log",
          # :| => " | ", #" 2>&1 | "
          # :> => " > ",
          #    :&& => " && "
          #mysql - with ecranned \", no table drawed and without column names
          mysql: "mysql smartback -sNe %S",
          onbeta: "ssh smartback.beta %B"
      }

      def |(cmd)
        self.class.pipe self, cmd
      end

      def &(cmd)
        self.class.new "(#{self}) & (#{cmd})"
      end


      def >(fname=nil)
        fname ||= self.class.tmpname()
        ret = self.class.new "#{self} > #{fname}"
        ret.output_at = fname
        ret
      end

      class << self

        def pipe(*cmds)
          new cmds.join Piper::PIPE
        end


        def tmpname
          '/tmp/' + (0...8).map { (65 + rand(26)).chr }.join
        end

        def named(cmd)
          fname = tmpname()
          ret = new "mkfifo #{fname} && #{cmd} > #{fname} && rm #{fname}"
          ret.output_at = fname
          ret
        end

        def intersect (cmd1, cmd2)
          cmd1 = named cmd1 | sort
          cmd1 & (cmd2 | sort | compare(cmd1.output_at))
        end


        def method_missing(name, *args)
          template(name, *args) ||
              plugin(name, *args) ||
              new(([name.to_s] + args).join ' ')
        end

        def respond_to_missing?(*)
          true
        end

        private

        def template(name, *params)
          return unless @@templates.has_key? name
          base = @@templates[name]
          #TODO: for now %% works incorrect
          base.to_enum(:scan, /%./).each_with_index do |holder, i|
            # pos = Regexp.last_match.offset(0)[0]+1
            # base[pos] = 's'
            params[i] = case holder
                          when '%S'
                            '"' + params[i].gsub('"', '\\"') + '"'
                          when '%B'
                            '\'' + params[i].gsub('\'', '\'\"\'\"\'') + '\''
                          else
                            params[i]
                        end
          end

          new base.gsub(/%./, "%s") % params
        end

        def plugin(name, *params)
          #up one dir and
          # @plugins_path ||= __FILE__.split(File.SEPARATOR)[0..-3] + ['plugins']
          #TODO: optimize disk usage - if no plugin folder should be checked only once
          #TODO: organize remote run via executing scripts inline
          fnames = Dir["plugins/#{name}.*"]
          new "#{File.absolute_path fnames.first} #{params.join ' '}" if fnames.length > 0
        end
      end
    end
  end
end

