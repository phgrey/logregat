require_relative 'command'

module Piper
  module Pipe
    class Line
      def initialize
        @tasks = []
      end


      def from_dsl(text)
        Piper::Command.instance_eval text
      end

      def run
        Dir.chdir('..') until File.exist? './Pipefile' || Dir.pwd == '/'
        instance_eval File.read './Pipefile'
        puts @tasks.last, `#{@tasks.last}` if @tasks.length > 0
      end

      def line(&block)
        @tasks.push Command.instance_eval &block
      end
    end
  end
end