-- https://otvet.mail.ru/question/2604655
-- http://www.zabaznov.ru/statyi/nauka/spacecom.html
-- https://ru.wikipedia.org/wiki/%D0%9F%D0%B5%D1%80%D0%B2%D0%B0%D1%8F_%D0%BA%D0%BE%D1%81%D0%BC%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F_%D1%81%D0%BA%D0%BE%D1%80%D0%BE%D1%81%D1%82%D1%8C
-- https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D1%81%D0%BC%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F_%D1%81%D0%BA%D0%BE%D1%80%D0%BE%D1%81%D1%82%D1%8C

import Prelude



--3d mode
--speed1 :: Float -> Float -> Float
speed1 (m,r) = sqrt (g*m/r)
  where
    g = 6.67408e-11

speed2 (m,r) = 2 * speed1 m r

main :: IO ()
main = do
  putStrLn ( show ( speed1 (3e53, 46.6e+9 * 9.46e15)))
  putStrLn ( show ( speed1 (3e53, 4.4e+26)))
