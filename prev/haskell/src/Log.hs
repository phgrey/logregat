
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

module Log where

--import Control.Applicative
import Data.Attoparsec.Char8
import Data.Word
import Data.Time


--logs datatypes

data LogLevel = Debug | Info | Warn | Close | Point
  deriving (Show, Eq)

data FlowStage = Got | Reject(String) | Accept | Operate(String)
    deriving (Show, Eq)

data IP = IP Word8 Word8 Word8 Word8 deriving Show

data LogEvent = Nothing
    | Worker {
            memail :: Maybe String,
            message :: String
        }
    | Accounts {
            emails :: [String],
            stage :: FlowStage
        }
    | UIDs {
            email :: String,
            uids :: [Int],
            stage :: FlowStage
        }
    | Email {
            email :: String,
            messageId :: String,
            uid :: Maybe Int,
            subject:: Maybe String,
            stage :: FlowStage
        }
    deriving (Show)

data LogMessage = FaultLog String
    | WorkerLog {
            level :: LogLevel,
            worker :: Int,
            host :: IP,
            event :: LogEvent,
            date :: UTCTime
        }
  deriving (Show, Eq)
instance Ord LogMessage where
  le1 <= le2 = date le1 <= date le2

--parsers

timeParser :: Parser UTCTime
timeParser = do
  _  <- count 3 char
  char ' '
  mm <- count 3 char
  char ' '
  d  <- count 2 digit
  char ' '
  y  <- count 4 digit
  char ' '
  h  <- count 2 digit
  char ':'
  m  <- count 2 digit
  char ':'
  s  <- count 2 digit
  string " GMT+0000 (UTC)"
  return $
    UTCTime { utctDay = fromGregorian (read y) (read mm) (read d)
              , utctDayTime = secondsToDiffTime (read h) * 3600 + (read m) * 60 + (read s)
                }

ipParser :: Parser IP
ipParser = do
  d1 <- decimal
  char '.'
  d2 <- decimal
  char '.'
  d3 <- decimal
  char '.'
  d4 <- decimal
  return $ IP d1 d2 d3 d4

levelParser :: Parser LogLevel
levelParser =
     (string "debug"    >> return Debug)
 <|> (string "info" >> return Info)
 <|> (string "warn"  >> return Warn)
 <|> (string "close"  >> return Close)
 <|> (string "point" >> return Point)

logParser :: Parser LogMessage
logParser = do
  l <- levelParser
  char ' '
  w <- decimal
  char ' '
  h <- ipParser
  char ' '
  e <- eventParser
  char ' '
  t <- timeParser
  return $ LogMessage l w h e t

eventParser :: Parser Event
eventParser = accountEventParser
  <|> uidEventParser
  <|> emailEventParser
  <|> workerEventParser

emailEventParser :: Parser Event
emailEventParser = do
  string "Got email "
  s <- emailStageParser
  char ' '
  e <- takeTill (==' ')
  char ' '







{-
TODO: benchmark the parsing options, this should be done with readTime
parseMessage :: String -> LogMessage
parseMessage s = case words s of
  -- I 5053 pci_id: con ing!
  ("I":t:msg) -> LogMessage Info (read t) (unwords msg)

  -- W 3654 e8] PGTT ASF!
  ("W":t:msg) -> LogMessage Warning (read t) (unwords msg)

  -- E 47 1034 'What a pity it
  ("E":sev:t:msg) -> LogMessage (Error $ read sev) (read t) (unwords msg)

  _ -> Unknown s

-}


{-
  regexp = c('^([a-z]+) *(\\d+) ([0-9.]+) (.*)((?: [^ ]+){7})$',
             '^([a-z]+) *(\\d+) ([0-9.]+) Got email on ([^ ]+) (.*?) ([^ ]+) (\\d+)(?: \\{.*\\})?((?: [^ ]+){7})$',
             '^([a-z]+) *(\\d+) ([0-9.]+) Got email before since ([^ ]+) (.*?) ([^ ]+)((?: [^ ]+){7})((?: [^ ]+){7})((?: [^ ]+){7})$',
             '^([a-z]+) *(\\d+) ([0-9.]+) Got email older than hour ([^ ]+) (.*?) ([^ ]+)((?: [^ ]+){7})((?: [^ ]+){7})((?: [^ ]+){7})$',
             '^([a-z]+) *(\\d+) ([0-9.]+) UIDS got true ([^ ]+) ([0-9 ]+)((?: [^ ]+){7})$',
             '^([a-z]+) *(\\d+) ([0-9.]+) UIDS got false ([^ ]+) ([0-9 ]+)((?: [^ ]+){7})$',
             '^([a-z]+) *(\\d+) ([0-9.]+) OVERKILL Outdated emails: ([0-9]+) (.*)((?: [^ ]+){7})$',
             '^([a-z]+) *(\\d+) ([0-9.]+) timeloss ms: (\\d+)((?: [^ ]+){7})$'),
             '^([a-z]+) Error while decoding \\[(.*)\\].*((?: [^ ]+){7})$',



-}
