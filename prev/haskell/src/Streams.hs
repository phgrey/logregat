module Streams (
     dummyLog
    ) where

import Data.List


type LogStream =  [String]
--dummy data from here
dummyLog :: LogStream
dummyLog = [ '[' : (show x)  ++ " - so are you happy " ++ y ++ " fakamaza?]"  | x <- [1..10], y <- ["little", "big"]]


data Event = Unknown
                | Little Int
                | Big Int

