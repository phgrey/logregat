module Sources (
     fromLog, fromStdIn
    ) where

import System.FSNotify
import Control.Concurrent (threadDelay)
import Control.Monad (forever)

import Data.Char

fromLog =
  withManager $ \mgr -> do
    -- start a watching job (in the background)
    watchDir
      mgr          -- manager
      "."          -- directory to watch
      (const True) -- predicate
      print        -- action

    -- sleep forever (until interrupted)
    forever $ threadDelay 1000000

--for testing purposes
--main = fromLog


fromStdIn = forever $ do
--            l <- getLine
            l <- getContents
            putStrLn $ (map toUpper l) ++ "|||"
