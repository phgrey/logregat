module Main where

import System.Environment( getArgs )
import Sources


main :: IO ()
main = getArgs >>= launch

launch :: [String] -> IO ()
launch ["stdin"] = fromStdIn
launch  _  = putStrLn "now exit"
