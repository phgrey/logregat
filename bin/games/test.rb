class A
  @@ololo = 1

  def echo
    p @@ololo
  end

  def self.echo
    p @@ololo
  end

end

A.new.echo
A.echo