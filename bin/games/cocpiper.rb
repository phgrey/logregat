require "cocaine"

def test
  line = Cocaine::CommandLine.new("git", "status")
  begin
    line.run
  rescue Cocaine::ExitStatusError => e
    e.message # => "Command 'git commit' returned 1. Expected 0"
  end
end

p test