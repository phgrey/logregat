require 'io/console'
def types
  p '------------------------------'
  p $stdout.tty? ? 'TTY' : 'NO'
  p '------------------------------'
  rows, columns = $stdout.winsize
  puts "Your screen is #{columns} wide and #{rows} tall"
rescue Errno::ENOTTY => e
  p e.inspect
  p e.class.name, e.message
end

require 'pty'
require 'expect'

def pingpong
  rp, wp = IO.pipe
  mesg = "ping "
  100.times {
    # IO.select follows IO#read.  Not the best way to use IO.select.
    rs, ws, = IO.select([rp], [wp])
    if r = rs[0]
      ret = r.read(5)
      print ret
      case ret
        when /ping/
          mesg = "pong\n"
        when /pong/
          mesg = "ping "
      end
    end
    if w = ws[0]
      w.write(mesg)
    end
  }
end

def autologin
PTY.spawn('sftp username@sftp.domain.com:/uploads') do |input, output|

  # Say yes to SSH fingerprint
  input.expect(/fingerprint/, 2) do |r|

    output.puts "yes" if !r.nil?

    # Enter SFTP password
    input.expect(/password/, 2) do |r|

      output.puts 'your_sftp_password' if !r.nil?

      input.expect(/sftp/) do

        # List folders and files in `/uploads`
        output.puts 'ls'

        # Check if folder named `foo` exist
        input.expect(/foo/, 1) do |result|

          is_folder_exist = result.nil? ? false : true
          # Create `foo` folder if does'nt exist
          output.puts "mkdir foo" if !is_folder_exist
          # Change directory to `foo`
          output.puts "cd foo"
          # Upload `/path/to/local/foo.txt` in `foo` folder as `foo.txt`
          output.puts "put /path/to/local/foo.txt foo.txt"
          # Exit SFTP
          output.puts "exit"

        end

      end

    end

  end

end
end