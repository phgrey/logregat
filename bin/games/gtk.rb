#https://github.com/cedlemo/ruby-gtk3-tutorial
require "gtk3"
require 'vte3'

module ToNote
  def pack1 term
    lab = Gtk::Label.new term.window_title
    append_page term, lab
    term.signal_connect("window-title-changed") { |widget| lab.text = widget.window_title}
  end
  alias pack2 pack1
end

Gtk::Notebook.include ToNote



def term
  vte = Vte::Terminal.new
  pid = vte.spawn({})
  vte.signal_connect("child-exited") { |widget| widget.parent.remove widget}
  # vte.signal_connect("commit") {|args, t| p args.inspect, t}
  # pid = vte.spawn :argv => GLib::Shell.parse(command_string)
  # :working_directory => working_dir,
  # :spawn_flags => GLib::Spawn::SEARCH_PATH)
  # p vte.pty.inspect
  vte
end



term1 = term
window.add term1
window.show_all




def replace what, with
  parent = what.parent
  parent.remove what
  with.pack1 what
  with.pack2 term
  parent.add with
  parent.show_all
end

def twin_hor what
  w = what.allocated_width
  replace what, Gtk::Paned.new(:horizontal)
  what.set_size_request w/2, what.allocated_height
end

def twin_ver what
  h = what.allocated_height
  replace what, Gtk::Paned.new(:vertical)
  what.set_size_request what.allocated_width, h/2
end

def twin_tab what
  replace what, Gtk::Notebook.new
end

Thread.abort_on_exception = true
# Thread.new{ sleep 2; twin_tab term1;}

Gtk.main