# require_relative '../lib/pipeline'
# require 'piper'
# Piper::Pipe::Line.new.run


class A
  @a = 'a'
  class << self
    attr_reader :a
  end

  # @
  def self.echo_a
    p a
  end

  def echo_b
    p self.class.a
  end
end

class B < A
  @a = 'b'
end

B.echo_a

B.new.echo_b

A.echo_a

A.new.echo_b